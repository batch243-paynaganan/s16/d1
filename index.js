// console.log("hello world")

// [Section] Arithmetic Operators

let x = 1397;
console.log("The value of x is: " + x);
let y = 7831;
console.log("The value of y is: " + y);

// Addition operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Difference operator
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Multiplication operator
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Division operator
let quotient = x / y;
console.log("Result of division operator: " + quotient)

// Modulo
let remainder = y % x;
console.log("Result of modulo: " + remainder);

let secondRemainder = x % y;
console.log("Result of modulo: " + secondRemainder);

// [Section] Assignment operators
	// Basic assignment operator (=)
	// the assignment operator adds the value of the right operand to a variable and assigns the result to the value.

	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition assignment operator
	// the addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	// assignmentNumber = assignmentNumber + 2;
	// console.log(assignmentNumber);
	assignmentNumber += 2;
	console.log("Result of Addition Assignment Operator: " + assignmentNumber);

	// subtraction assignment operator (-=)
	assignmentNumber -=2;
	console.log( "Result of Subtraction Assignment Operator: " + assignmentNumber);

	// multiplication assignment operator (*=)
	assignmentNumber *=4;
	console.log( "Result of Muliplication Assignment Operator: " + assignmentNumber);

	// division assignment operator (/=)
	assignmentNumber /=8;
	console.log( "Result of Division Assignment Operator: " + assignmentNumber);


// [Section] Multiple operators and parentheses
 	
 	let mdas = 1 + 2 - 3 * 4 / 5;
 	console.log("Result of MDAS rule: " + mdas);

 	let pemdas = 1 + (2 - 3) * (4 / 5);
 	console.log("Result of PEMDAS rule: " + pemdas);

// [Section] Increment and Decrement
	// operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement

	let z = 1;

	// pre-increment
	let increment = ++z;
	console.log("Result of z in pre-increment: " + z);
	console.log("Result of increment in pre-increment: " + increment);

	// post-increment
	increment = z++;
	console.log("Result of z in post-increment: " + z);
	console.log("Result of increment in post-increment: " + increment);
	increment = z++;
	console.log("Result of increment in post-increment: " + increment);

	let p = 0;

	// pre-decrement
	let decrement = --p;
	console.log("Result of p in post-decrement: " + p);
	console.log("Result of decrement in post-decrement: " + decrement);

	// post-decrement
	decrement = p--;
	console.log("Result of p in post-decrement: " + p);
	console.log("Result of decrement in post-decrement: " + decrement);

	// post-decrement
	decrement = p--;
	console.log("Result of decrement in post-decrement: " + decrement);

// [Section] Type Coercion
	/*
		-Type coercion is the automatic or implicit conversion of values from one data type to another.
		-This happens when one operations are performed on different data types that would normally not be possible and yield irregular results.
		-values are automatically converted from one data type to another in order to resolve operations.
	*/

	let numA = '10';
	let numB = 12;
		/*
			-adding or concatinating a string and a number will result into string
			-this can be proven in the console by looking at the color of the text
		*/
	// coercion
	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	// non coercion
	let numC = 16;
	let numD = 14;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	let numE = true + 1;
	console.log(numE);
	/*
		Type coercion: boolean value and number
		the results will be a number
		the boolean value of true and false will be converted into 1 and 0 consecutively.
	*/

// [Section] Comparison Operators
	let juan = 'juan';

	// equality operators (==)
	/*
		-checks whether the operands are equal/ have the same value
	*/
	let isEqual = 1 == 1;
	console.log(typeof isEqual);

	console.log(1 == 2);
	console.log(1 == '1');

	// Strict equality operator (===)
	console.log(1 === '1');
	console.log('juan' == 'juan');
	console.log('juan' == juan);

	// inequality operator
	/*
		-checks whether the operands are not equal/ have different content
		-attempts to convert and compare operands of different data types
	*/

	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');

	// strict inequality operator(!==)
	console.log(1 !== '1');

// [Section] Relational operators
	// some comparison operators check whether one value is greater or less than to the other value.

	let a = 50;
	let b = 65;

	// GT or greater than operator (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan);

	// LT or less than operator (<)
	let isLessThan = a < b;
	console.log(isLessThan);

	// GTE or greater than or equal operator (>=)
	let isGTOrEqual = a >= b;
	console.log(isGTOrEqual);

	// LTE or less than or equal operator (<=)
	let isLTOrEqual = a <= b;
	console.log(isLTOrEqual);

	let numStr = "30";
	console.log(a > numStr);
	console.log(b <= numStr);


// [Section] Logical operators
	let isLegalAge = true;
	let isRegistered = false;

	// logical And operator (&&)
	// returns true if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical And operator: " + allRequirementsMet);

	// Logical Or operator (||)
	// returns true if one of the operand is true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical Or operator: " + someRequirementsMet);

	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical Not operator: " + someRequirementsNotMet);

	